﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDLib
{
    public class BluffMaster : Player
    {
        private MonteCarloRollOut monte;
        private const int attempts = 100;
        private const float threshold = .5f;
        public BluffMaster(int startingDiceCount) : base(startingDiceCount)
        {
            Name = "Flex Master Bluff";
            monte = new MonteCarloRollOut(attempts, threshold);
        }

        public override async Task<PlayerMove> TakeTurn(PlayerMove lastMove)
        {
            if (lastMove == null)
                return new PlayerMove { GuessAmount = 1, DiceNumber = hand[0]};
            var move = await monte.WinChance(lastMove, hand, GameState.Instance.TotalDice);
            if (move.WinPercent < threshold)
            {
                return new PlayerMove() { Call = true };
            }
            var nextMove = new PlayerMove() { DiceNumber = lastMove.DiceNumber, GuessAmount = lastMove.GuessAmount + 1 };
            return nextMove;
        }
    }
}
