﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDLib
{
    public class HumanPlayer : Player
    {
        public HumanPlayer(int startingDiceCount):base(startingDiceCount)
        {
            Human = true;
        }
        public override async Task<PlayerMove> TakeTurn(PlayerMove lastMove)
        {
            return new PlayerMove() { Human = true };
        }

        public override void Roll()
        {
            base.Roll();
            //var builder = new StringBuilder();
            //builder.AppendFormat("{0} rolled ", Name);
            //foreach (var dice in hand)
            //    builder.AppendFormat("{0} ", dice);
            //Console.WriteLine(builder);
        }
    }
}
