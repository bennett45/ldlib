﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDLib
{
    public class PlayerMove
    {
        public bool Call { get; set; }

        public int DiceNumber { get; set; }

        public float WinPercent { get; set; }

        public int GuessAmount { get; set; }
        public bool Human { get; set; }

        public override string ToString()
        {
            if (Call)
                return "Call!";
            return string.Format("{0} {1}'s", GuessAmount, DiceNumber);
        }

    }
}
