﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDLib
{
    public class MaybeBluff : Player
    {
        private Random rand;
        private MonteCarloRollOut monte;
        private const int attempts = 100;
        private const float threshold = .5f;
        private const float BLUFF_CHANCE = .3f;
        public MaybeBluff(int startingDiceCount) : base(startingDiceCount)
        {
            Name = "Fib Maybies";
            rand = new Random();
            monte = new MonteCarloRollOut(attempts, threshold);
        }

        public override async Task<PlayerMove> TakeTurn(PlayerMove lastMove)
        {
            if (lastMove == null)
                return await PlayMonteCarlo(lastMove);
            if (rand.Next(0, 100)/100 > BLUFF_CHANCE)
                return await PlayMonteCarlo(lastMove);
            return await PlayBluff(lastMove);  
        }

        private async Task<PlayerMove> PlayBluff(PlayerMove lastMove)
        {
            var move = new PlayerMove() { DiceNumber = lastMove.DiceNumber, GuessAmount = lastMove.GuessAmount+1 };
            if (GameState.Instance.ValidMove(move))
                return move;
            return await PlayMonteCarlo(lastMove);
        }

        private async Task<PlayerMove> PlayMonteCarlo(PlayerMove lastMove)
        {
            var moveList = GetMoves(lastMove);
            var move = new List<PlayerMove>(await Task.WhenAll(moveList));
            move.Sort((move1, move2) => move2.WinPercent.CompareTo(move1.WinPercent));
            return move[0];
        }

        private List<Task<PlayerMove>> GetMoves(PlayerMove lastMove)
        {
            if (lastMove == null)
                lastMove = new PlayerMove() { GuessAmount = 1, DiceNumber = 1 };
            var moves = new List<Task<PlayerMove>>();
            lastMove.Call = true;
            moves.Add(monte.WinChance(lastMove, hand, GameState.Instance.TotalDice));
            for (int i = lastMove.GuessAmount; i < GameState.Instance.TotalDice; ++i)
            {
                for (int j = 1; j < 7; ++j)
                {
                    if (i == lastMove.GuessAmount && j <= lastMove.DiceNumber)
                        continue;
                    var move = new PlayerMove { DiceNumber = j, GuessAmount = i };
                    moves.Add(monte.WinChance(move, hand, GameState.Instance.TotalDice));
                }
            }
            return moves;
        }
    }
}
