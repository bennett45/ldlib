﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDLib
{
    public static class Roller
    {
        static Random rand = new Random();

        public static int RollDice()
        {
            return rand.Next(1, 7);
        }

        public static List<int> RollHand(int numOfDice)
        {
            ///this code sucks
            var hand = new List<int>();
            for(int i = 0; i < numOfDice; ++i)
            {
                hand.Add(RollDice());
            }
            return hand;
        }
    }
}
