﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDLib
{
    public class RoundResult
    {
        public Player Winner { get; set; }
        public Player Loser { get; set; }

        private DiceCounter counter;

        public RoundResult(Player callingPlayer, Player guessingPlayer, List<int> dice, PlayerMove lastMove, bool onesWild)
        {
            counter = new DiceCounter(onesWild);
            if (counter.CorrectGuess(dice, lastMove))
            {
                Winner = guessingPlayer;
                Loser = callingPlayer;
            }
            else
            {
                Loser = guessingPlayer;
                Winner = callingPlayer;
            }
        }

        public override string ToString()
        {
            return string.Format("{0} wins the round. {1} loses a dice.", Winner, Loser);
        }
    }
}
