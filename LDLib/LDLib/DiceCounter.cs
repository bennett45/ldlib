﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDLib
{
    public class DiceCounter
    {
        public DiceCounter(bool OnesWild)
        {
            this.OnesWild = OnesWild;
        }

        public bool OnesWild { get; set; }

        public bool CorrectGuess(List<int> dice, PlayerMove guess)
        {
            if(OnesWild)
                return guess.GuessAmount <= dice.Where(d => d == guess.DiceNumber || d == 1).Count();
            return guess.GuessAmount <= dice.Where(d => d == guess.DiceNumber).Count();
        }
    }
}
