﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDLib
{
    public abstract class Player
    {
        public List<int> hand;
        public int currentDice;
        public bool Human { get; set; }

        public string Name { get; set; }

        public int Wins { get; set; }
        public Player(int startingDiceCount)
        {
            Name = PlayerNamer.GetRandomName();
            currentDice = startingDiceCount;
            hand = new List<int>(startingDiceCount);
            Console.WriteLine("this is a player");
        }

        public abstract Task<PlayerMove> TakeTurn(PlayerMove lastMove);

        public override string ToString()
        {
            return Name;
        }

        public virtual void Roll()
        {
            hand = Roller.RollHand(currentDice);
        }

        public string OutputPlayersRoll()
        {
            var builder = new StringBuilder();
            builder.AppendFormat("{0} rolled ", Name);
            foreach (var dice in hand)
                builder.AppendFormat("{0} ", dice);
            return builder.ToString();
        }
    }

}
