﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDLib
{
    public class RandomMonte: Player
    {
        private MonteCarloRollOut monte;
        private const int ATTEMPTS = 100;
        private const float THRESHOLD = .5f;
        private const int RND_RNG = 5;
        public RandomMonte(int startingDiceCount) : base(startingDiceCount)
        {
            Name = "Crazy Monte";
            monte = new MonteCarloRollOut(ATTEMPTS, THRESHOLD);
        }

        public override async Task<PlayerMove> TakeTurn(PlayerMove lastMove)
        {
            var moveList = GetMoves(lastMove);
            var move = new List<PlayerMove>(await Task.WhenAll(moveList));
            move.Sort((move1, move2) => move2.WinPercent.CompareTo(move1.WinPercent));
            return move[0];
        }

        private List<Task<PlayerMove>> GetMoves(PlayerMove lastMove)
        {
            if (lastMove == null)
                lastMove = new PlayerMove() { GuessAmount = 1, DiceNumber = 1 };
            var moves = new List<Task<PlayerMove>>();
            lastMove.Call = true;
            moves.Add(monte.WinChance(lastMove, hand, GameState.Instance.TotalDice));
            for (int i = lastMove.GuessAmount; i < GameState.Instance.TotalDice; ++i)
            {
                for (int j = 1; j < 7; ++j)
                {
                    if (i == lastMove.GuessAmount && j <= lastMove.DiceNumber)
                        continue;
                    var move = new PlayerMove { DiceNumber = j, GuessAmount = i };
                    moves.Add(monte.WinChance(move, hand, GameState.Instance.TotalDice, RND_RNG));
                }
            }
            return moves;
        }
    }
}
