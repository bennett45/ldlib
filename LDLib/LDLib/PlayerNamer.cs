﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDLib
{
    public class PlayerNamer
    {
        private static Random rand = new Random();

        private static List<string> playerNames = new List<string>()
        {
            "Cousin Cornelius",
            "Frank",
            "Shavon",
            "Geno",
            "Trey",
            "Sophie",
            "Roy Barnett",
            "Rufus",
            "Pepper",
            "Cher",
            "Freaky",
        };

        public static string GetRandomName()
        {
            var number = rand.Next(0, playerNames.Count);
            var name = playerNames[number];
            playerNames.Remove(name);
            return name;
        }
    }
}
