﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDLib
{
    public class BasicComputerPlayer : Player
    {
        public BasicComputerPlayer(int startDiceCount):base(startDiceCount)
        {
            Name = "Gary";
        }
        public override async Task<PlayerMove> TakeTurn(PlayerMove lastMove)
        {
            if (lastMove == null)
                return new PlayerMove { GuessAmount = 1, DiceNumber = 1 };
            return new PlayerMove() { GuessAmount = ++lastMove.GuessAmount, DiceNumber = lastMove.DiceNumber };
        }
    }
}
