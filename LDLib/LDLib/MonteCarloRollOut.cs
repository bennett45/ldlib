﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDLib
{
    public class MonteCarloRollOut
    {
        private int attempts;
        private float threshold;
        private DiceCounter counter;
        private Random rand;

        public MonteCarloRollOut(int attempts, float threshold)
        {
            this.attempts = attempts;
            this.threshold = threshold;
            rand = new Random();
            counter = new DiceCounter(GameState.Instance.OnesWild);
        }

        public async Task<PlayerMove> WinChance(PlayerMove move, List<int> hand, int totalDice, int randRange = 0)
        {
            int wins = 0;
            for(int i = 0; i < attempts; ++i)
            {
                var dice = Roller.RollHand(totalDice - hand.Count);
                dice.AddRange(hand);
                if (counter.CorrectGuess(dice, move))
                    ++wins;
            }

            move.WinPercent = (float)wins / attempts;
            move.WinPercent += rand.Next(-randRange, randRange) / 100;
            if (move.Call)
                move.WinPercent = 1 - move.WinPercent;
            return move;
        }

    }
}
