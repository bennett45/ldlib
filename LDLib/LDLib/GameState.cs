﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDLib
{
    public class GameState
    {
        private static GameState instance;
        private Queue<Player> turnQueue;
        private Stack<PlayerMove> moveStack;
        private DiceCounter counter;

        public List<Player> Players
        {
            get { return turnQueue.ToList(); }
        }
        public int TotalDice
        {
            get
            {
                int count = 0;
                foreach (var player in turnQueue)
                    count += player.hand.Count;
                return count;
            }
        }

        public int TotalPlayers
        {
            get { return turnQueue.Count; }
        }

        public bool OnesWild { get; set; }
        public PlayerMove LastMove
        {
            get { return moveStack.Count == 0 ? null : moveStack.Peek(); }
        }


        public static GameState Instance
        {
            get
            {
                if (instance == null)
                    instance = new GameState();
                return instance;
            }
        }

        public bool GameOver { get { return turnQueue.Count(p=>p.currentDice > 0) == 1; } }

        public bool RoundOver { get; set; }
        public Player CurrentTurn { get { return turnQueue.Count > 0 ? turnQueue.Peek() : null; } }

        public bool Rolling { get; set; }

        private GameState()
        {
            OnesWild = true;
            turnQueue = new Queue<Player>();
            moveStack = new Stack<PlayerMove>();
            counter = new DiceCounter(OnesWild);
        }

        public void AddPlayer(Player newPlayer)
        {
            turnQueue.Enqueue(newPlayer);
        }

        public void RollOut()
        {
            foreach(var player in turnQueue)
            {
                player.Roll();
            }
        }

        public async Task<PlayerMove> Play()
        {
            Rolling = false;
            while (turnQueue.Peek().currentDice == 0)
                EndTurn();
            var tempMove = await turnQueue.Peek().TakeTurn(LastMove);
            if (!tempMove.Human)
                moveStack.Push(tempMove);
            return tempMove;
        }

        public void EndTurn()
        {
            var temp = turnQueue.Dequeue();
            turnQueue.Enqueue(temp);
        }

        public RoundResult EndRound()
        {
            RoundOver = true;
            List<int> dice = new List<int>();
            foreach (var player in turnQueue)
                dice.AddRange(player.hand);
            var result = new RoundResult(turnQueue.Peek(), turnQueue.Where(p => p.currentDice > 0).Last(), dice, LastMove, OnesWild);
            moveStack = new Stack<PlayerMove>();
            foreach (var player in turnQueue)
                player.hand = new List<int>();
            result.Loser.currentDice--;
            if(GameOver)
            {
                result.Winner.Wins++;
            }
            Rolling = true;
            return result;
        }

        public bool ValidMove(PlayerMove move)
        {
            if (LastMove == null && move.Call)
                return false;
            if (move.Call)
                return true;
            if (move.DiceNumber > 6 || move.DiceNumber < 1)
                return false;
            if (LastMove == null || move.GuessAmount > LastMove.GuessAmount)
                return true;
            return move.GuessAmount == LastMove.GuessAmount && move.DiceNumber > LastMove.DiceNumber;
        }

        public void AddMove(PlayerMove move)
        {
            moveStack.Push(move);
        }

        public string GetAllRolls()
        {
            var builder = new StringBuilder();
            foreach (var player in turnQueue)
                builder.AppendFormat("{0} \n", player.OutputPlayersRoll());
            return builder.ToString();
        }

        public void PlayAgain()
        {
            foreach (var player in turnQueue)
            {
                player.currentDice = 5;
            }
        }    
        
        public bool AllPlayersRolled()
        {
            return turnQueue.Count(p => p.hand.Count == p.currentDice) == turnQueue.Count;
        }
            
    }
}
